
import Graphics.Gloss
import System.Random(randomIO)

main = do { seed <- (randomIO :: IO Int);
			simulate (InWindow "My Window" (1000, 1000) (10, 10)) (greyN 0.2) 1 (initialState seed 100 100) frameUpdate logicUpdate} 
			
frameUpdate state = globalTransformation $ drawList 100 0 drawColumn state
	where
		globalTransformation p = translate (-500) (-500) $ scale 0.1 0.1 p
		drawColumn column = drawList 0 100 drawCell column
			where
				frame = color black $ rectangleWire 100 100
				aliveCell = color (dark orange) $ rectangleSolid 100 100
				drawCell cell = if cell then pictures[aliveCell, frame] else frame

logicUpdate viewPort time state = [ [ cell (isAlive x y) (neighbours x y) | y <- [0 .. 99] ] | x <- [0 .. 99] ] 
	where
		cell alive n = (n == 3) || (alive && n == 2)
		isAlive x y = (state !! x) !! y
		neighbours x y = length [(nx,ny) | nx <- [x-1 .. x+1], ny <- [y-1 .. y+1], nx >= 0, ny >= 0, nx < 100, ny < 100, (nx /= x || ny /= y), isAlive nx ny]

drawList dx dy drawItem list = (pictures . map f . indexList) list
	where
		f (index, item) = translate ((fromIntegral index)*dx) ((fromIntegral index)*dy) $ drawItem item
		indexList list = zip [0 .. (length list)-1] list

initialState seed width height = [[ even (x*seed `mod` y)   | y <- [1..height]] | x <- [1..width]] 